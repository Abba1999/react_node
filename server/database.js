const Sequelize = require('sequelize');
const config =require('./configs/db');

const sequelize = new Sequelize({
    database: config.database,
    username: config.username,
    password: config.password,
    host: config.host,
    port: config.port,
    dialect: config.dialect,
    dialectOptions: config.dialectOptions,
    logging: false,
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.recipe = require("./src/models/Recipe.model.js")(db.sequelize,db.Sequelize);
db.user = require("./src/models/Users.model.js")(db.sequelize,db.Sequelize);
db.social = require("./src/models/Social.model")(db.sequelize,db.Sequelize);
db.category = require("./src/models/Category.model")(db.sequelize,db.Sequelize);

sequelize.authenticate()
    .then(() => console.log('Database connected...'))
    .catch(err => console.log('Error: ' + err));

module.exports = db;
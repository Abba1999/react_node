const express = require('express');
const cors = require("cors");
const app = express();

var corsOptions={
  origin: "http://localhost:3000"
}
app.use(cors(corsOptions));
app.use(express.json());

const db = require("./database.js");
db.sequelize.sync();

app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","x-access-token, Origin, Content-Type, Accept", "*");
    next();
});

const recipes = require("./src/controllers/Recipe.controller.js");
const users = require("./src/controllers/User.controller");
const verifyLogin = require('./src/middleware/verifyLogin.js');
const verifyToken = require('./src/middleware/verifyToken.js');

//recipes
app.post('/addRecipe/',verifyToken.authUser,recipes.create);
app.get('/getRecipes/',recipes.findAll);
app.get('/recipes/:id', recipes.findUsersRecipe);
app.delete('/recipes/:id',verifyToken.authUser,recipes.delete);

app.get('/getRecipes/:id',recipes.findOne);

//users
app.post('/login/',users.signin);
app.post('/register/',verifyLogin.checkDuplicateEmail,users.signup);
app.get('/allUsers/', users.findAll);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

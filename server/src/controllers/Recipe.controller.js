const db = require("../../database");
const Recipe = db.recipe;

//create a recipe
exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    const recipe = {
        name: req.body.name,
        description: req.body.description,
        image: req.body.image,
        weight: req.body.weight,
        calories: req.body.calories,
        fat: req.body.fat,
        protein: req.body.protein,
        carbs: req.body.carbs,
        id_user: req.body.id_user
    };

    
    const id_1 = req.body.meat;
    const id_2 = req.body.dish;

    Recipe.create(recipe)
        .then(data => {
            const category = {
                id_recipe: data.dataValues.id_recipe,
                id_category: id_1,
            }
            db.category.create(category)
            .then(data =>{
            }).catch(err => {
                console.log(err);
            });
            category.id_category = id_2;
            db.category.create(category)
            .then(data =>{
            }).catch(err => {
                console.log(err);
            });
            res.send(data);
            

        }).catch(err => {
            res.status(500).send({
                message: err.message || "Error occured while uploading the Recipe,"
            });
        });
};

//get all Recipes
exports.findAll = (req, res) => {
    Recipe.findAll()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message
            });
        });
};

//get users all Recipes
exports.findUsersRecipe = (req, res) => {
    const id = req.params.id;
    Recipe.findAll({
        where: { id_user: id }
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

//find by id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Recipe.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            response.status(500).send({
                message: err.message
            });
        });
};

//delete recipe with given id
exports.delete = (req, res) => {
    const id = req.params.id;
    const id_user = req.userId;
    db.user.findOne({
        where: {
            id_user: id_user
        }
    }).then(data => {
        if (!data) {
            return res.status(404).send({ message: "User not fouund" });
        } 
        if (data.role != 1) {
            return res.status(401).send({ message: "You are not admin" });
        } 
    }).catch(err => {
        console.log(err);
    });

    Recipe.destroy({
        where: { id_recipe: id }
    }).then(num => {
        if (num == 1) {
            res.send(num);
            res.send({
                messege: "Recipe deleted successfylly!"
            });
        } else {
            res.send({
                message: "Canot delete Recipe with id=" + id
            });
        }
    }).catch(err => {
        return res.status(401).send({ message: "Problem with deleting recipe" });
    });
};


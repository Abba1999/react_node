const db = require("../../database");
const Social = db.social;

exports.createSocial = (req, res) => {

    const social = {
        nick: req.body.nick,
        id_social: req.body.id_user_new,
        id_user: req.body.id_user_new
    };

    Social.create(social)
        .then(data => {
            res.send(req.body);
        }).catch(err => {
            console.log(err.message);
            res.status(500).send({
                message: err.message || "Error occured while uploading the Recipe,"
            });
        });
};

exports.getSocial = (req,res) => {
    const id_social = req;
    Social.findOne({
        where: {
            id_user: id_social
        }}).then(data => {
            return data.dataValues.nick;
        }).catch(err => {
            console.log(err);
            });
}

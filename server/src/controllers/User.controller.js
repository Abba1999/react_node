const { user } = require("../../database");
const db = require("../../database");
const crypto = require("crypto");
const User = db.user;
const SocialCon = require("../controllers/Social.controller");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const secrets = require("../../configs/var");

//register
exports.signup = (req, res) => {
    let buff = crypto.randomBytes(16);
    let salt = buff.toString('base64');

    const user = {
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password + salt, 8),
        salt: salt,
        role: 0,
    };
    User.create(user)
        .then(data => {
            req.body.id_user_new = data.id_user;
            SocialCon.createSocial(req, res);
        })
        .catch(err => {
            console.log(err.message);
            res.status(500).send({
                message: err.message || "Error occured while uploading the Recipe,"
            });
        });
};

//login
exports.signin = (req, res) => {

    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(data => {
        if (!data) {
            return res.status(404).send({ message: "User not fouund" });
        }

        const passwordValidation = bcrypt.compareSync(
            req.body.password + data.salt,
            data.password
        );

        if (!passwordValidation) {
            res.send({
                logged: "no"
            });
        }

        const token = jwt.sign({ id: data.id }, secrets.user, { expiresIn: 86400 });

        res.send({
            logged: "yes",
            id: data.id_user,
            token: token,
            role: data.role
        });
    }).catch(err => {
        response.status(500).send({
            message: "Error user"
        });
    });
};


exports.findAll = (req, res) => {
    User.findAll()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Error occurred while retrieving recipes."
            });
        });
};




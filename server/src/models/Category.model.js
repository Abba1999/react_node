module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define("Recipe_Categories", {
    id_recipe:{
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    id_category:{
        type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    modelName: 'Recipe_Categories',
    freezeTableName: true,
    timestamps: false  
  });
  
    return Category;
};
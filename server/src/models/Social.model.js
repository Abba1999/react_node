module.exports = (sequelize, DataTypes) => {
    const Social = sequelize.define("Social", {
    id_social:{
        type: DataTypes.INTEGER,
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    id_user:{
        type: DataTypes.INTEGER,
    },
    nick:{
        type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    modelName: 'Users',
    freezeTableName: true,
    timestamps: false  
  });
  
    return Social;
};
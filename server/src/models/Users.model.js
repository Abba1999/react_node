module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define("Users", {
    id_user:{
        type: DataTypes.INTEGER,
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    email:{
        type: DataTypes.STRING,
    },
    password:{
        type: DataTypes.STRING,
    },
    salt:{
        type: DataTypes.STRING,
    },
    role:{
        type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    modelName: 'Users',
    freezeTableName: true,
    timestamps: false  
  });
  
    return Users;
};
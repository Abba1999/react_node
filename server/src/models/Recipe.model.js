module.exports = (sequelize, DataTypes) => {
    const Recipe = sequelize.define("Recipe", {
    id_recipe:{
        type: DataTypes.INTEGER,
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    name:{
        type: DataTypes.STRING,
    },
    description:{
        type: DataTypes.STRING,
    },
    image:{
        type: DataTypes.STRING,
    },
    weight:{
        type: DataTypes.INTEGER,
    },
    calories:{
        type: DataTypes.INTEGER,
    },
    fat:{
        type: DataTypes.INTEGER,
    },
    protein:{
        type: DataTypes.INTEGER,
    },
    carbs:{
        type: DataTypes.INTEGER,
    },
    created_at:{
        type: DataTypes.DATE,
    },
    stars:{
        type: DataTypes.INTEGER,
    },
    id_user:{
        type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    modelName: 'Recipe',
    freezeTableName: true,
    timestamps: false  
  });
  
    return Recipe;
};
const db = require("../../database");
const User = db.user;
const jwt = require("jsonwebtoken");
const secret = require("../../configs/var");


authUser = (req, res, next) => {
  const token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }
  jwt.verify(token, secret.user, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id;
    next();
  });
};

const verifyToken = {
  authUser: authUser,
};
module.exports = verifyToken;
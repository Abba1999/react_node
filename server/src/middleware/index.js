const verifyLogin = require("./verifyLogin");
const verifyToken = require("./verifyToken");

module.exports = {
    verifyLogin,
    verifyToken
};
import React, { Component } from "react";
import "../css/Login.css";
import "../css/Forum.css";
import axios from "axios";
import authentication from "../scripts/authentication";
import { storage } from "../config/firebase";

class addRecipe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      method: '',
      weight: 0,
      calories: 0,
      fat: 0,
      carbs: 0,
      protein: 0,
      image: '',
      meat: 1,
      dish: 5,
      description: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  handleSubmit(event) {

    const recipe = {
      name: this.state.title,
      description: this.state.description,
      weight: this.state.weight,
      calories: this.state.calories,
      fat: this.state.fat,
      carbs: this.state.carbs,
      protein: this.state.protein,
      image: this.state.image,
      id_user: authentication.getCurrentUser(),
      meat: this.state.meat,
      dish: this.state.dish
    }
    if (recipe.fat < 0 || recipe.calories < 0 || recipe.carbs < 0 || recipe.protein < 0 || recipe.weight < 0) {
      alert("Negative values. Change!");
    } else {
      if (recipe.id_user === null || recipe.title === null) {
        alert("You are not logged in!");
        this.props.history.push('/');
      } else {
        axios.post(`http://localhost:8080/addRecipe/`, recipe, { headers: authentication.authHeader() })
          .then(res => {
            alert("You have added recipe!");
          });
        event.preventDefault();
      }
    }
  }

  render() {

    return (
      <form className="loginRegisterForm" onSubmit={this.handleSubmit}>
        <h2>Add Recipe</h2>
        <p>Title</p>
        <input className="loginRegisterFormInput" type="text" name="title" id="title" placeholder="Baked Potatoes" onChange={this.handleChange} />
        <p>Method</p>
        <textarea className="loginRegisterFormInput" type="text" name="description" id="description" placeholder="1. Peel potatoes 2. ..." onChange={this.handleChange} />
        <p>Weight [g]</p>
        <input className="loginRegisterFormInput" type="number" name="weight" id="weight" placeholder="10" onChange={this.handleChange} />
        <p>Calories [kcal]</p>
        <input className="loginRegisterFormInput" type="number" name="calories" id="calories" placeholder="100" onChange={this.handleChange} />
        <p>Fat [mg]</p>
        <input className="loginRegisterFormInput" type="number" name="fat" id="fat" placeholder="5" onChange={this.handleChange} />
        <p>Carbs [mg]]</p>
        <input className="loginRegisterFormInput" type="number" name="carbs" id="carbs" placeholder="8" onChange={this.handleChange} />
        <p>Protein [mg]</p>
        <input className="loginRegisterFormInput" type="number" name="protein" id="protein" placeholder="12" onChange={this.handleChange} />
        <p>Image</p>
        <input className="loginRegisterFormInput" type="file" name="image" id="image" onChange={this.handleChange} />
        <p>Meat</p>
        <select className="loginRegisterFormInput" name="meat" value={this.state.meat} onChange={this.handleChange} >
          <option value="1" >Fish</option>
          <option value="2" >Beef</option>
          <option value="3" >Pork</option>
          <option value="4" >Nope</option>
        </select>
        <p>Kind</p>
        <select className="loginRegisterFormInput" name="dish" value={this.state.dish} onChange={this.handleChange} >
          <option value="5" >Soup</option>
          <option value="6" >Dish</option>
          <option value="7" >Dessert</option>
        </select>
        <input className="loginRegisterSubmit" type="submit" value="Submit" />
      </form>
    );
  }
}

export default addRecipe;

import React, { Component } from "react";
import axios from "axios";
import SelectChange from "../component/SelectModel";
import authentication from "../scripts/authentication";

class DeleteRecipe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipeId: '',
            recipes: [],
            };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        axios.get(`http://localhost:8080/getRecipes/`)
            .then(res => {
                const recipes = res.data;
                this.setState({ recipes });
                const recipeId = res.data[0].id_recipe;
                this.setState({ recipeId });
                if (!recipeId){
                    alert("No recipes");
                    this.props.history.push('/');
                }
            });
    }

    handleChange = ({ target }) => {
        const recipeId = target.value;
        this.setState({ recipeId });
    };

    handleSubmit(event) {

        const url = `http://localhost:8080/recipes/` + this.state.recipeId;
        axios.delete(url,{ headers: authentication.authHeader() })
            .then(res => {
                const token = res.data;
                if (token !== "blad") {
                    alert("Recipe deleted");
                    window.location.reload();
                } else {
                    throw Error("Recipe not deleted");
                }
            })
            .catch(res => {
                if(authentication.getCurrentUser()){
                    alert("Deleted recipe");
                } else{
                    alert("Error");
                }
                
            });
        event.preventDefault();
    }

    render() {
        return (
            <div className="loginRegister">
                <form className="loginRegisterForm" onSubmit={this.handleSubmit} style={{minWidth: "25em", minHeight: "13em"}}>
                    <h2 >Choose recipe to delete</h2>
                    <select className="loginRegisterFormInput" value={this.state.recipeId} onChange={this.handleChange} >
                        {this.state.recipes.map(recipe => (
                            <SelectChange name={recipe.id_recipe + " " + recipe.name} id={recipe.id_recipe} />
                        ))}
                    </select>
                    <input className="loginRegisterSubmit" type="submit" value="Usuń produkt" />
                </form>
            </div>
        );
    }
}

export default DeleteRecipe;
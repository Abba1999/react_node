import React, { Component } from "react";
import "../css/Login.css"
import axios from "axios";

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  logout() {
    localStorage.removeItem("user");
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }


  handleSubmit(event) {

    const user = {
      email: this.state.email,
      password: this.state.password
    }
    console.log(user);
    axios.post(`http://localhost:8080/login/`, user)
      .then(res => {
        const data = res.data;
        console.log(data);
        if(res.data.logged === "yes"){
          localStorage.setItem("token", JSON.stringify(res.data.token));
          localStorage.setItem("id", JSON.stringify(res.data.id));
          localStorage.setItem("role", JSON.stringify(res.data.role));
          localStorage.setItem("nick",JSON.stringify(res.data.nick));
          alert("You have logged in");
          this.props.history.push('/');
          window.location.reload();
        } else{
          alert("Wrong password or email. Try again.");
        }
        
      });
    event.preventDefault();
  }

  render() {

    return (
      <form className="loginRegisterForm" onSubmit={this.handleSubmit}>
        <input className="loginRegisterFormInput" type="text" name="email" id="email" placeholder="E-mail" onChange={this.handleChange} />
        <input className="loginRegisterFormInput" type="password" name="password" id="password" placeholder="Hasło" onChange={this.handleChange} />
        <input className="loginRegisterSubmit" type="submit" value="Login" />
      </form>
    );
  }
}

export default Login;


import React, { Component } from "react";
import "../css/Login.css"
import axios from "axios";

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordAgain: '',
      nick: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }


  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };



  handleSubmit(event) {

    const user = {
      email: this.state.email,
      password: this.state.password,
      nick: this.state.nick
    }
    console.log(user);
    axios.post(`http://localhost:8080/register/`, user)
      .then(res => {
        const token = res.data;
        console.log(token);
        alert("You have signed up!");
        this.props.history.push('/login');
      });
    event.preventDefault();
  }

  render() {

    return (
      <form className="loginRegisterForm" onSubmit={this.handleSubmit}>
        <input className="loginRegisterFormInput" type="text" name="email" id="email" placeholder="E-mail" onChange={this.handleChange} />
        <input className="loginRegisterFormInput" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange} />
        <input className="loginRegisterFormInput" type="password" name="password" id="password" placeholder="Password again" onChange={this.handleChange} />
        <input className="loginRegisterFormInput" type="text" name="nick" id="nick" placeholder="Ncik" onChange={this.handleChange} />
        <input className="loginRegisterSubmit" type="submit" value="Register" />
      </form>
    );
  }
}

export default Register;


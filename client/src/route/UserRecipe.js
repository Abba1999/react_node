
import React, { Component } from "react";
import Recipe from "../component/Recipe";
import axios from "axios";

class UserRecipes extends Component {
    state = {
        recipes: []
    }

    componentDidMount() {
        let id = localStorage.getItem("id");
        let url = `http://localhost:8080/recipes/` + id;
        axios.get(url)
            .then(res => {
                const recipes = res.data;
                if(recipes.length === 0){
                    alert("You didn't add any recipe");
                }
                this.setState({ recipes });
            });
    }

    render() {

        return (
            <div className="Recipes">
                {this.state.recipes.map(recipe => (
                    <Recipe title={recipe.name} description={recipe.description} weight={recipe.weight} calories={recipe.calories}
                        fat={recipe.fat} carbs={recipe.carbs} protein={recipe.protein} img={recipe.img} />
                ))}
            </div>
        );
    }
}

export default UserRecipes;
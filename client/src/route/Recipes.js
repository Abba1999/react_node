
import React, { Component } from "react";
import Recipe from "../component/Recipe";
import axios from "axios";

class Recipes extends Component{
  state = {
    recipes: []
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/getRecipes/`)
      .then(res => {
        const recipes = res.data;
        this.setState({ recipes });
      });
  }
    
  render(){
  
  return(
    <div className="Recipes">
      {this.state.recipes.map(recipe => (
        <Recipe title={recipe.name} description={recipe.description} weight={recipe.weight} calories={recipe.calories} 
          fat={recipe.fat} carbs={recipe.carbs} protein={recipe.protein} img={recipe.img}/>
        ))}
    </div>
    );
  }
}
  
export default Recipes;
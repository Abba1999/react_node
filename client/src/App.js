import React, { Component } from "react";
import "./css/App.css";
import Menu from "./component/Menu";
import addRecipe from "./route/addRecipe";
import Recipes from "./route/Recipes";
import Login from "./route/Login";
import Register from "./route/Register";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import UserRecipes from "./route/UserRecipe";
import DeleteRecipe from "./route/deleteRecipe";


class App extends Component {

  render(){

  return(
      <div className="App">
        <Router>
          <Menu />
          <Switch>
            <Route path="/" exact component={Recipes} />
            <Route path="/addRecipe" component={addRecipe} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/UserRecpies" component={UserRecipes} />
            <Route path="/deleteRecipe" component={DeleteRecipe} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;

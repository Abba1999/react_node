import React from "react";
import style from "../css/recipe.module.css"
const Recipe = ({title, description, weight, calories, fat, carbs, protein, img}) => {
    return(
        <div className={style.recipe}>
            <h1>{title}</h1>
            <p>Recipe: {description}</p>
            <p>Weight: {weight}g</p>
            <p>Calories: {calories}kcal</p>
            <p>Fat: {fat}mg</p>
            <p>Carbs: {carbs}mg</p>
            <p>Protein: {protein}mg</p>
            <img src={img} alt="" />
        </div>
    ); 
};


export default Recipe;
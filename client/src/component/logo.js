import React, { Component } from "react";
import logo_img from "../img/logo.png";

class Logo extends Component{
    render(){
        return(
            <div className="logodiv">
                <img className="logo" src={logo_img} alt="Logo" />
            </div>

        );
    }
}

export default Logo;
import React, { Component } from "react";
import Logo from "./logo";
import styled from "styled-components";
import { Link } from "react-router-dom";
import "../css/Menu.css"
import authentication from "../scripts/authentication";

const Button = styled.button`
  background-color: transparent;
  color:white;
  font-size: 1em;
  paddign: inherit;
  text-align: left;
  text-decoration: none;
  display: flex;
  align-items: center;
  height: 100%;
  border: none;
  :hover {
    background-color: #424242;
    cursor: pointer;
}
`;

const SubmitButton = styled(Button)`
  background-color: #b2ff59;
  color:black;
  border: none;
  border-radius: 0 0.5em 0.5em 0;
  :hover {
    background-color: #86c143;
}
`;

class Menu extends Component{
    
    render(){

    return(
        <header className="Menu">
            
            <form className="UpperMenu">
                
                <input className="searchBar" type="text" />
                <SubmitButton className="searchButton">
                    Seach
                </SubmitButton>

            </form>
           
            <Button to="/" as={Link} className ="ImgButton">
                <Logo />
            </Button>
            {authentication.getCurrentUser() &&<Button to="/addRecipe" as={Link}>
                Add Recipe
            </Button>}
            {authentication.getCurrentUser() && <Button to="/UserRecpies" as={Link}>
                Recipes
            </Button>}
            {authentication.getCurrentUser() && authentication.getUserRole() === 1 && <Button to="/deleteRecipe" as={Link}>
                Delete
            </Button>}
            {authentication.getCurrentUser() && <Button className="LoginButton" onClick={authentication.logout}>
                Log out
            </Button>}
            {!authentication.getCurrentUser() && <Button to="/login" className="LoginButton" as={Link}>
                Sign in
            </Button>}
            {!authentication.getCurrentUser() && <Button to="/register" as={Link}>
                Register
            </Button>}
            
            
        </header>
        ); 
    }
}


export default Menu;
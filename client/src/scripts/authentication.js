const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    console.log("logged out");
    window.location.reload();
}

const getToken = () => {
    return JSON.parse(localStorage.getItem("token"));
}

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("id"));
};

const getUserRole = () => {
    return JSON.parse(localStorage.getItem("role"));
}

const authHeader = () => {
  const user = getToken();

  if (user) {
    console.log(user);
    return { 'x-access-token': user};
  } else {
    return {};
  }
};


export default {
    getToken,
    logout,
    getCurrentUser,
    getUserRole,
    authHeader
};